package com.dexciuq.timer

enum class Constants {
    SECONDS, SECONDS_REMAINING, IS_RUNNING, TIMER_PAUSED, PAUSED_TIME
}