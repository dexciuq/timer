package com.dexciuq.timer

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dexciuq.timer.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupListeners()
    }

    private fun setupListeners() {
        with(binding) {
            sendTextBtn.setOnClickListener {
                val seconds = secondsInputLayout.editText?.text.toString()
                if (seconds.isBlank()) {
                    secondsInputLayout.error = getString(R.string.field_must_be_filled)
                    return@setOnClickListener
                } else {
                    secondsInputLayout.error = null
                }

                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                intent.putExtra(Intent.EXTRA_TEXT, seconds.toLong())
                startActivity(intent)
            }

            startTimerBtn.setOnClickListener {
                val seconds = secondsInputLayout.editText?.text.toString()
                if (seconds.isBlank()) {
                    secondsInputLayout.error = getString(R.string.field_must_be_filled)
                    return@setOnClickListener
                } else {
                    secondsInputLayout.error = null
                }

                val intent = Intent(this@MainActivity, TimerActivity::class.java)
                intent.putExtra(Constants.SECONDS.name, seconds.toLong())
                startActivity(intent)
            }
        }
    }
}