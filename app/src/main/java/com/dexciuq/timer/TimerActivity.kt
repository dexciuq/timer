package com.dexciuq.timer

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.dexciuq.timer.databinding.ActivityTimerBinding

class TimerActivity : AppCompatActivity() {

    private val binding by lazy { ActivityTimerBinding.inflate(layoutInflater) }
    private lateinit var countDownTimer: CountDownTimer
    private var seconds: Long = 0
    private var secondsRemaining: Long = 0
    private var isRunning: Boolean = false
    private var timerPaused = false
    private var pausedTime: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        if (savedInstanceState != null) {
            seconds = savedInstanceState.getLong(Constants.SECONDS.name)
            secondsRemaining = savedInstanceState.getLong(Constants.SECONDS_REMAINING.name)
            isRunning = savedInstanceState.getBoolean(Constants.IS_RUNNING.name)
            timerPaused = savedInstanceState.getBoolean(Constants.TIMER_PAUSED.name)
            pausedTime = savedInstanceState.getLong(Constants.PAUSED_TIME.name)

            if (isRunning) {
                countDownTimer = if (timerPaused) {
                    createCountDownTimer(pausedTime)
                } else {
                    createCountDownTimer(secondsRemaining)
                }
                countDownTimer.start()
            }
        } else {
            seconds = if (intent.action == Intent.ACTION_SEND) {
                intent.getLongExtra(Intent.EXTRA_TEXT, 0)
            } else {
                intent.getLongExtra(Constants.SECONDS.name, 0)
            }

            countDownTimer = createCountDownTimer(seconds)
            secondsRemaining = seconds
        }

        with(binding) {
            startBtn.setOnClickListener {
                if (timerPaused) {
                    countDownTimer = createCountDownTimer(pausedTime)
                    timerPaused = false
                } else {
                    countDownTimer = createCountDownTimer(secondsRemaining)
                }
                countDownTimer.start()
                isRunning = true
                updateUI()
            }

            pauseBtn.setOnClickListener {
                countDownTimer.cancel()
                pausedTime = secondsRemaining
                timerPaused = true
                isRunning = false
                updateUI()
            }

            resetBtn.setOnClickListener {
                countDownTimer.cancel()
                secondsRemaining = seconds
                timerPaused = false
                isRunning = false
                updateUI()
            }

            updateUI()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(Constants.SECONDS.name, seconds)
        outState.putLong(Constants.SECONDS_REMAINING.name, secondsRemaining)
        outState.putBoolean(Constants.IS_RUNNING.name, isRunning)
        outState.putBoolean(Constants.TIMER_PAUSED.name, timerPaused)
        outState.putLong(Constants.PAUSED_TIME.name, pausedTime)
    }

    private fun createCountDownTimer(seconds: Long): CountDownTimer {
        return object : CountDownTimer(seconds * 1000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                secondsRemaining = millisUntilFinished / 1000
                updateUI()
            }

            override fun onFinish() {
                secondsRemaining = 0
                isRunning = false
                updateUI()
            }
        }
    }

    private fun updateUI() {
        with(binding) {
            countDown.text = String.format("%02d:%02d:%02d",
                secondsRemaining / 3600, // hours
                (secondsRemaining % 3600) / 60, // minutes
                secondsRemaining % 60 // seconds
            )
            startBtn.isVisible = secondsRemaining > 0 && !isRunning
            pauseBtn.isVisible = isRunning
            resetBtn.isVisible = secondsRemaining > 0 || isRunning
        }
    }
}